import tensorflow as tf
import cv2
from collections import deque

from drmario import DrMario as Game


GAME = Game.name
ACTIONS = 6
GAMMA = 0.99
OBSERVE = 600
EXPLORE = 600
FINAL_EPSILON = 0.05
INITIAL_EPSILON = 1.0
REPLAY_MEMORY = 600000
BATCH = 32
K = 1


def weight_variable():
    ...

def bias_variable():
    ...

def conv2d():
    ...

def max_pool_2x2(x):
    ...

def create_network():
    ...

def train_network():
    ...

def play_game():
    ...
