import numpy as np
import time
from PIL import Image
import cv2

from elmchey import Elmchey, config, GameError


BYTEORDER, SIGNED = config.BYTEORDER, config.SIGNED


class DrMario():

    game = 'Dr. Mario'

    def __init__(self):
        self.debugger = Elmchey()
        self.debugger.connect()
        if not self.debugger.info['game'] == self.game:
            self.debugger._fail()
            raise GameError('Wrong game run!')
        time.sleep(5) # Grey-screen otherwise.
        self.debugger.run()
        self._stepper = self.debugger.frame_stepper()
        self.last_score = 0
        self.init_game()



    @property
    def score(self):
        """Get current game score."""
        start, end = 0x984C, 0x9852
        score = []
        entry = self.debugger.read_memory(start, end - start)
        for digit in entry:
            if int(digit) in range(10):
                score.append(str(digit))
        return int(''.join(score + ['0']))

    @property
    def menu(self):
        """Check whether current screen is menu."""
        start, end =  0x9983, 0x9984
        values = (0xFE, 0xC, 0x83, 0x93)
        message = self.debugger.read_memory(start, end - start) or False
        value = int.from_bytes(message, BYTEORDER, signed = SIGNED)
        return value in values

    @property
    def terminal(self):
        """Check whether game has terminated."""
        return (self.menu == True) and (self.score == 0)

    @property
    def screen(self):
        """Get current screen as image."""
        image_bytes = self.debugger.screen_buffer()
        pil_image = Image.frombytes('RGB', (160, 144), image_bytes)
        cv2_image = np.array(pil_image)
        cv2_image = cv2_image[:, :, ::-1].copy()
        return cv2_image

    def init_game(self):
        """Initialize game."""
        next(self._stepper)
        while self.menu:
            self.debugger.button('START', True)
            next(self._stepper)


    def frame_step(self, action):
        """Take step and do actions."""

        if self.terminal:
            screen = self.screen
            reward = self.score - self.last_score
            terminal = True
            self.init_game()
        else:
            while self.menu:
                self.debugger.button('START', True)
                next(self._stepper)

            if action[1] == 1:
                self.button('RIGHT', True)
            elif action[2] == 1:
                self.button('LEFT', True)
            elif action[3] == 1:
                self.button('UP', True)
            elif action[4] == 1:
                self.button('DOWN', True)
            elif action[5] == 1:
                self.button('A', True)
            elif action[6] == 1:
                self.button('B', True)

            next(self._stepper)

            screen = self.screen
            reward = self.score - self.last_score
            terminal = False

        return screen, reward, terminal
